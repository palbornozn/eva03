package cl.inacap.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table (name="copialibros")

@NamedQueries({
	@NamedQuery(name="CopiaLibros.getAll",query="select c from CopiaLibros c")
	
})
public class CopiaLibros implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_copiaLibros;
	
	@ManyToOne 
	@JoinColumn(name="cod_isbn")
	Libro libro;
	
	@Column(name="numeroSerie")
	private int numeroSerie;
	
	@Column(name="destino")
	private String destino;
	
	@ManyToOne
	@JoinColumn(name="id_precioLibro")
	PrecioLibro precioLibro;
	
	@Column(name="id_estadoLibro")
	private int id_estadoLibro;
	
	@Column(name="estado_registro")
	private boolean estado_registro;
	
	
	
	public int getId_copiaLibros() {
		return id_copiaLibros;
	}
	public void setId_copiaLibros(int id_copiaLibros) {
		this.id_copiaLibros = id_copiaLibros;
	}

	public Libro getLibro() {
		return libro;
	}
	public void setLibro(Libro libro) {
		this.libro = libro;
	}
	public int getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(int numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}

	public PrecioLibro getPrecioLibro() {
		return precioLibro;
	}
	public void setPrecioLibro(PrecioLibro precioLibro) {
		this.precioLibro = precioLibro;
	}
	public int getId_estadoLibro() {
		return id_estadoLibro;
	}
	public void setId_estadoLibro(int id_estadoLibro) {
		this.id_estadoLibro = id_estadoLibro;
	}
	public boolean isEstado_registro() {
		return estado_registro;
	}
	public void setEstado_registro(boolean estado_registro) {
		this.estado_registro = estado_registro;
	}

}
