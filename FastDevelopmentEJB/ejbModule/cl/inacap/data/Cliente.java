package cl.inacap.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table (name="cliente")

@NamedQueries({
	@NamedQuery(name="Cliente.getAll",query="select c from Cliente c")
	
})
public class Cliente implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_cliente;
	
	@Column(name="DNI") 
	private String DNI;
	
	@Column(name="nombre") 
	private String nombre;
	
	@Column(name="apellidos")
	private String apellidos;
	
	@Column(name="fechaNacimiento")
	private Date fechaNacimiento;
	
	@Column(name="estado_registro") 
	private boolean estado_registro;
	
	
	public int getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
	public String getDNI() {
		return DNI;
	}
	public void setDNI(String dNI) {
		DNI = dNI;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public boolean isEstado_registro() {
		return estado_registro;
	}
	public void setEstado_registro(boolean estado_registro) {
		this.estado_registro = estado_registro;
	}
	
	

}
