package Service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import cl.inacap.data.PrecioLibro;

/**
 * Session Bean implementation class PrecioLibroService
 */
@Stateless
@LocalBean
public class PrecioLibroService implements PrecioLibroServiceLocal {

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("FastDevelopmentEJB");
    /**
     * Default constructor. 
     */
    public PrecioLibroService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void insertPrecio(PrecioLibro p) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			
			em.persist(p);
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
	}

	@Override
	public List<PrecioLibro> listaPrecios() {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			return em.createNamedQuery("PrecioLibro.getAll",PrecioLibro.class).getResultList();
			
			}catch(Exception ex) {
				return null;
				}finally {
					em.close();
				}
	}

	@Override
	public void deletePrecioLibro(PrecioLibro p) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			em.remove(em.find(PrecioLibro.class, p.getId_precioLibro()));
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
		
	}

	@Override
	public void updatePrecioLibro(PrecioLibro p) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			PrecioLibro original = em.find(PrecioLibro.class, p.getId_precioLibro());
				
			original.setCod_isbn(p.getCod_isbn());
			original.setEstado_registro(p.isEstado_registro());
			original.setFechaModificacion(p.getFechaModificacion());
			original.setId_precioLibro(p.getId_precioLibro());
			original.setId_tipoPrecio(p.getId_tipoPrecio());
			original.setPrecio(p.getPrecio());
			
			em.merge(original);
			em.flush();
					
					
					
		}catch(Exception ex) {
			
		}finally {
			em.close();
		}
	}

}
