package Service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import cl.inacap.data.Usuarios;

/**
 * Session Bean implementation class UsuariosService
 */
@Stateless
@LocalBean
public class UsuariosService implements UsuariosServiceLocal {
	
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("FastDevelopmentEJB");
    /**
     * Default constructor. 
     */
    public UsuariosService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void insertUsuarios(Usuarios u) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			em.persist(u);
			em.flush();
			
		}catch(Exception ex) {
			
		}finally {
			em.close();
		}
	}

	@Override
	public List<Usuarios> listarUsuarios() {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			return em.createNamedQuery("Usuarios.getAll",Usuarios.class).getResultList();
		}catch(Exception ex) {
		return null;
		}finally {
			em.close();
		}
	}

	@Override
	public void deleteUsuarios(Usuarios u) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			em.remove(em.find(Usuarios.class, u.getId_usuario()));
			em.flush();
		}catch(Exception ex) {
			
		}finally {
			em.close();
		}
	}

	@Override
	public void updateUsuarios(Usuarios u) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			Usuarios original = em.find(Usuarios.class, u.getId_usuario());
			
			
			original.setContraseña(u.getContraseña());
			original.setEstado_registro(u.isEstado_registro());
			original.setId_tipoUsuario(u.getId_tipoUsuario());
			original.setId_trabajador(u.getId_trabajador());
			original.setId_usuario(u.getId_usuario());
			original.setNombreUsuario(u.getNombreUsuario());
			
		}catch(Exception ex) {
			
		}finally {
			em.close();
		}
		
	}

}
