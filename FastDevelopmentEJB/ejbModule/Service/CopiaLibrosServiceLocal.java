package Service;

import java.util.List;

import javax.ejb.Local;

import cl.inacap.data.CopiaLibros;

@Local
public interface CopiaLibrosServiceLocal {
	
	public void insertCopialibros(CopiaLibros cl);
	public List<CopiaLibros> listaCopiaLibros();
	public void deleteCopiaLibros(CopiaLibros cl);
	public void updateCopuaLibros(CopiaLibros cl);
	

}
