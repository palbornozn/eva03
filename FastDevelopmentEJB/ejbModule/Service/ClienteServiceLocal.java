package Service;

import java.util.List;
import javax.ejb.Local;
import cl.inacap.data.Cliente;

@Local
public interface ClienteServiceLocal {
	
	
	
	public void insertCliente(Cliente c);
	
	public List<Cliente>listarCliente();
	
	public void deleteCliente(Cliente c);
	
	public void updateCliente(Cliente c);
	

}
