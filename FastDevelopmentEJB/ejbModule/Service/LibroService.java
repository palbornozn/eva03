package Service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import cl.inacap.data.Libro;

/**
 * Session Bean implementation class LibroService
 */
@Stateless
@LocalBean
public class LibroService implements LibroServiceLocal {
	
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("FastDevelopmentEJB");
    /**
     * Default constructor. 
     */
    public LibroService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void insertLibro(Libro l) {
		// TODO Auto-generated method stub
		EntityManager em= this.emf.createEntityManager();
		try {
			
			em.persist(l);
			em.flush();
		}catch(Exception ex) {
			
		}finally {
			em.close();
		}
		
	}

	@Override
	public List<Libro> ListasLibro() {
		// TODO Auto-generated method stub
		EntityManager em =this.emf.createEntityManager();
		try {
			return em.createNamedQuery("Libro.getAll",Libro.class).getResultList();
		}catch(Exception ex){
			System.out.println("nada");
			return null;
		}finally {
			em.close();
		}
	}

	@Override
	public void deleteLibro(Libro l) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			em.remove(em.find(Libro.class, l.getCod_isbn()));
			em.flush();
		}catch(Exception ex) {
			
		}finally {
			em.close();
		}
	}

	@Override
	public void updateLibro(Libro l) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			Libro original = em.find(Libro.class, l.getCod_isbn());
			
			original.setA�o_publicacion(l.getA�o_publicacion());
			original.setCod_isbn(l.getCod_isbn());
			original.setContador_serie(l.getContador_serie());
			original.setEstado_registro(l.isEstado_registro());
			original.setId_editorial(l.getId_editorial());
			original.setId_idioma(l.getId_idioma());
			original.setPaginas(l.getPaginas());
			original.setTitulo(l.getTitulo());
			
			em.merge(original);
			em.flush();
			
		}catch(Exception ex) {
			
		}finally {
			em.close();
		}
	}

}
