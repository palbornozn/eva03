package Service;

import java.util.List;

import javax.ejb.Local;

import cl.inacap.data.Libro;

@Local
public interface LibroServiceLocal {
	
	public void insertLibro(Libro l);
	public List<Libro>ListasLibro();
	public void deleteLibro(Libro l);
	public void updateLibro(Libro l);

}
