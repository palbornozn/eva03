package Service;

import java.util.List;

import javax.ejb.Local;

import cl.inacap.data.Usuarios;

@Local
public interface UsuariosServiceLocal {
	
	public void insertUsuarios(Usuarios u);
	
	public List<Usuarios> listarUsuarios();
	
	public void deleteUsuarios(Usuarios u);
	
	public void updateUsuarios(Usuarios u);

}
