package Service;

import java.util.List;

import javax.ejb.Local;

import cl.inacap.data.PrecioLibro;

@Local
public interface PrecioLibroServiceLocal {
	
	public void insertPrecio(PrecioLibro p);
	public List<PrecioLibro> listaPrecios();
	public void deletePrecioLibro(PrecioLibro p);
	public void updatePrecioLibro(PrecioLibro p);

}
