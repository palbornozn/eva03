package cl.inacap.FastDevelopment.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Service.ClienteServiceLocal;
import Service.CopiaLibrosServiceLocal;

import cl.inacap.data.Cliente;
import cl.inacap.data.CopiaLibros;



/**
 * Servlet implementation class Vender
 */
@WebServlet("/Vender.do")
public class Vender extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vender() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Inject 
    private ClienteServiceLocal cs;
    @Inject
    private CopiaLibrosServiceLocal cl;
   
    
    
    public static Date formatoFecha(String fecha) {
    	try {
     return new	SimpleDateFormat("yyyy-MM-dd").parse(fecha);
    	}catch(Exception ex) {
    		return null;
    	}
    }
    	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
			
	    List<CopiaLibros> copiaLibros = new ArrayList<>();
		copiaLibros = cl.listaCopiaLibros();
		
		request.setAttribute("copiaLibros",copiaLibros);
		request.getRequestDispatcher("Site/Vender.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		
		
		String opc = request.getParameter("opc");
		
		if(opc.equals("1")) {
		Date fechaNacimiento =  formatoFecha(request.getParameter("fechaNacimiento").toString());
		Cliente c = new Cliente();
		

		c.setNombre(request.getParameter("nombre"));
		c.setApellidos(request.getParameter("apellido"));
		c.setFechaNacimiento(fechaNacimiento);
		c.setDNI(request.getParameter("rutPersona"));
		cs.insertCliente(c);
		
		
		response.sendRedirect("Vender.do");
		
		
		}
		
		
		
		
		if(opc.equals("2")) {
		
		response.setContentType("application/json:charset=UTF-8");	
		int isbn = Integer.parseInt(request.getParameter("isbn"));
			
		List<CopiaLibros> copiaLibros = new ArrayList<>();
		copiaLibros = cl.listaCopiaLibros();
		String titulo ="";
		int precio =0;
		
		
		String concatena = "{";
		
		for( CopiaLibros clb: copiaLibros) {
			
			if(isbn == clb.getLibro().getCod_isbn()){
				 
				 concatena += " 'titulo:' " +  clb.getLibro().getTitulo() + ", 'precio':" + clb.getPrecioLibro().getPrecio();
				break;
			}
		}
		
		concatena += "}";
		
		
		response.getWriter().write(concatena);
		
		
		/*
		response.setContentType("application/json:charset=UTF-8");
		int isbn = Integer.parseInt(request.getParameter("isbn"));

		PrintWriter out = response.getWriter();
		List<CopiaLibros> copiaLibros = new ArrayList<>();
		copiaLibros = cl.listaCopiaLibros();
		out.print(copiaLibros);
		out.flush();
		*/
		
		
		}
		
		

		
	
						
	}

}
