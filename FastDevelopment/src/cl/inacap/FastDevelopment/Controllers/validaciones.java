package cl.inacap.FastDevelopment.Controllers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Service.ClienteServiceLocal;
import Service.LibroServiceLocal;
import Service.TrabajadorServiceLocal;
import cl.inacap.data.Cliente;
import cl.inacap.data.Libro;
import cl.inacap.data.Trabajador;

/**
 * Servlet implementation class validaciones
 */
@WebServlet("/validaciones.do")
public class validaciones extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public validaciones() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Inject 
    private ClienteServiceLocal cs;
    @Inject
    private LibroServiceLocal ls;
    @Inject
    private TrabajadorServiceLocal ts;
    public static Date formatoFecha(String fecha) {
    	try {
     return new	SimpleDateFormat("yyyy-MM-dd").parse(fecha);
    	}catch(Exception ex) {
    		return null;
    	}
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unlikely-arg-type")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		
		String opc = request.getParameter("opc");
		
		
		if(opc.equals("1")){
			
			List<Cliente> listaCliente = new ArrayList<>();
			listaCliente = cs.listarCliente();
			
			 String rut = request.getParameter("rutPersona");
			 boolean valida = false;
			 
				 for(Cliente d : listaCliente ) {
					 
					 if(rut.equals(d.getDNI())) {
						  valida = true;
						  break;
					 }
				 }
				 				 			  
					response.getWriter().print((valida)?"1":"-1");
					response.flushBuffer();
			 
		}else if(opc.equals("2")) {
		
			List<Libro> listaLibro = new ArrayList<>();
			listaLibro = ls.ListasLibro();
			int isbn = Integer.parseInt(request.getParameter("isbn"));
			boolean valida = false;
			
				for(Libro l: listaLibro) {
					
					if(isbn == (l.getCod_isbn())) {
						valida =true;
						break;
					}
				}
				
			 response.getWriter().print((valida)?"1":"-1");
		
		}else if(opc.equals("3")) {
			
			List<Trabajador> listaTrabajador = new ArrayList<>();
			listaTrabajador = ts.listaTrabajador();
			String rut = request.getParameter("rut");
			boolean valida = false;
			
				for(Trabajador t : listaTrabajador) {
					if(rut.equals(t.getDNI())) {
						valida = true;
						break;
					}
				}
				
				response.getWriter().print((valida)?"1":"-1");
			 
		}	
			
		
		
		
	}

}
