<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FastDevelopment</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href="Site/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
     <link href="Site/demo/demo.css" rel="stylesheet" />
    
    <script defer src="Site/fontawesome/js/all.js"></script> 
    <link rel="stylesheet" href="Site/css/style.css" type="text/css">

 

 

</head>

<body>


	<jsp:include page="Header.jsp"/>
    



    <section>
        <!--  Ultimo libro vendido   -->
        <div class="row">

            <div class="col-md-12">

                <div class="row">

                    <div class="col-md-4">
                        <div class="card card-stats">
                            <div class="card-header card-header-info card-header-icon">
                              <div class="card-icon">
                                <i class="material-icons">L</i>
                              </div>
                              <p class="card-category">Ultima venta</p>
                              <h3 class="card-title">$20.000</h3>
                            </div>
                            <div class="card-footer">
                              <div class="stats">
                                <i class="material-icons">date_range</i> actualizado hace 5min
                              </div>
                            </div>
                          </div>

                </div>

            </div>

       <!-- Button trigger modal -->

<button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
    Agregar Cliente
  </button>
  
  <!-- Modal -->

  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Complete El Formulario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
              </button>
                </div>


        <div class="modal-body">
         
          <!--   FORMULARIO  DE INGRESO NUEVO USUARIO -->
            <form class="needs-validation" id="formUsuarioNuevo" >
                <div class="form-row">
                  <div class="col-md-6 mb-3">
                    <label for="validationCustom01">Nombre</label>
                      <input type="text" class="form-control" onchange="validarNombre(this.value)" name="usuarioNuevoNombre" id="usuarioNuevoNombre" >
                        </div>

                  <div class="col-md-6 mb-3">
                    <label for="validationCustom02">Apellidos</label>
                      <input type="text" class="form-control" onchange="validarApellido(this.value)" name="usuarioNuevoApellido" id="usuarioNuevoApellido" >
                        </div>
                </div>

                <div class="form-row">
                  <div class="col-md-6 mb-3">
                    <label for="validationCustom03">Fecha Nacimiento</label>
                      <input type="date" id="fechaNacimiento" name="fechaNacimiento"  min="1950-01-01" max="2022-12-31" >
                        </div>
	
                  <div class="col-md-3 mb-3">
                    <label for="validationCustom05">Rut</label>
                     <input type="text" class="form-control" name="rutNuevoUsuario" onchange="validaRut(this.value)" id="rutNuevoUsuario" >             
                      </div>
                        </div>
                
                    <button class="btn btn-primary" id="btnAgregarPersona"  type="button">Agregar</button>
              </form>


        </div>
        <div class="modal-footer">

          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
           </div>
          </div>
        </div>
       </div>
      </div>

        <!-- Formulario de venta -->

        <div class="container">
            <div class="card">
          
              <div class="col-md-8 col-centered">
                <div class="input-group">
                  <span class="input-group-text"  >Rut</span>  
                    <input type="text" class="form-control" id="rutVenta" onchange="validaRut(this.value)" placeholder="17652265-5" >
                     </div>
                       </div>


                <div class="col-md-8 col-centered">
                  <div class="input-group">
                    <span class="input-group-text" >Cod ISBN</span>  
                      <input type="text" class="form-control" onchange="validaISBN(this.value)" id="isbnVenta" placeholder="BN544551" >
                        </div>         
                          </div>

                <div class="col-md-8 text-center col-centered">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <label class="input-group-text" for="selectMetodo">Metodo de pago</label>
                        </div>
                           <select class="custom-select" id="inputGroupSelect01">
                            <!--option selected id="selecMetodo">Seleccione</option-->
                            <option >Efectivo</option>
                            <option >Debito</option>
                            <option >Credito</option>
                           </select>
                            </div>
                             </div>

                              <div class="col-md-8 text-center col-centered">
                              <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <label class="input-group-text" for="inputGroupSelect01">Cantidad</label>
                                    </div>
                                       <select class="custom-select" id="cantidad">
                                        <!--option selected id="selectCant">Seleccione</option-->
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                       </select>
                                        </div>
                                         </div>
                                         
                    <div class="col-md-12 text-center">
                      <button class="btn btn-primary" type="button"  id="btnAgregar"  >Agregar</button>
                        </div>
              </div>    
             

          <!-- DETALLE DE LA VENTA -->

     <div class="card">

            <div class="col-md-12 text-center">
              <h5>Detalle de venta</h5>
                </div>

                <div class="col-md-12">
                  <table class="table table-light table-striped">
                      <thead>
                         <tr>
                             <th>ISBN</th>
                             <th>Nombre Libro</th>
                             <th>Cantidad</th>
                             <th>Precio</th>                       
                         </tr>
                      </thead>
      
                      <tbody class="tbody" name="bodySales">    
                          </tbody>
                      </table>
               </div>
                       
               <div class="col-md-12">
                 <hr>
                  <table class="table table-light table-striped">
                    <thead>
                     <tr>
                      <th>Forma de pago</th>
                        <th>Monto</th>                      
                         </tr>
                          </thead>
  
                  <tbody class="tbody" name="bodyTotal">    
                      </tbody>
                          </table>
               </div>
               <div class="col-md-12 text-center">
                <button class="btn btn-primary" >VENDER</button>
                  </div>
                  </div>

                

     </div>






    </section>

</body>


  

<script src ="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="Site/js/main.js"></script>
<script src="Site/js/vender.js"></script>
<script src="Site/js/Validaciones.js"></script>
<script src ="Site/js/notification/js/bootstrap-msg.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

</html>