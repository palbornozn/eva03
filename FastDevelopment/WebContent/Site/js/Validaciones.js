

/*DESHABILITA BOTON FORMULARIO */

	function deshabilitarForm(boolean){
			if(boolean){
			  $('#btnAgregarPersona').prop('disabled',false);
			}else{
			  $('#btnAgregarPersona').prop('disabled',true);
					}
 		 	}


  
/*VALIDA NOMBRE*/ 

	function validarApellido(apellido){
		if(apellido.length<4){
		 
			alert("Apellido Invalido")
			$("#usuarioNuevoApellido").val("");
			deshabilitarForm(false)
  
		  }else if(apellido.includes(".")){
			alert("El nombre contiene puntos");
			$("#usuarioNuevoApellido").val("");
			deshabilitarForm(false)
			  
			}else{
			  deshabilitarForm(true)
			  	}
	  		}



/*VALIDA APELLIDO*/

	function validarNombre(nombre){
		if(nombre.length<4){
  
			alert("nombre invalido");
			$("#usuarioNuevoNombre").val("");
			deshabilitarForm(false)
  
			} else if(nombre.includes(".")){
			  alert("El nombre contiene puntos");
			  $("#usuarioNuevoNombre").val("");
			  deshabilitarForm(false)
			  }
				else{
				  deshabilitarForm(true)
				}
	  		}
  

/* VERIFICA QUE EL ISBN EXISTA EN LA BD */

function validaISBN(isbn){
	
	if(isbn.length<=7){
		$("#btnAgregar").prop('disabled',true)
		alert("isbn invalido")
	}else{
		$("#btnAgregar").prop('disabled',false)
		dataISBN = {
			opc:2,
			isbn: isbn
			}
	
				$.ajax({
					
					url:"validaciones.do",
					type:"POST",
					data: dataISBN,
					success:function(response){
						if(response=="1"){
							console.log("Libro existente en BD")
						}else{
							console.log("libro no existe en BD")
						}
						
						}
					})
	}
 }




/* VERIFICA QUE EL USUARIO ESTE CREADO EN LA VENTANA DE VENTA */

function rutVenta(rut){
	
	dataRutVenta = {
		opc:1,
		rutPersona:rut}
	
		$.ajax({
			
			url:"validaciones.do",
			type:"POST",
			data: dataRutVenta,
			success:function(response){
				if(response=="-1"){
					alert("El cliente no existe, favor agregar")
					$("#btnAgregar").prop('disabled',false)
					
				}else{
					console.log("Cliente existente")
					deshabilitarForm(false)
					
				}
			 }
		})
 }



/*VALIDA QUE EL TRABAJADOR ESTE CREADO*/


function validaTrabajador(rut){
	
	data ={
		rut: rut,
		opc:3
	   }
	
		$.ajax({
			
			url:"validaciones.do",
			type:"POST",
			data: data,
			success: function(response){
				if(response == "1"){
					console.log("trabajador ya existe")
					$("#btnAgregarTrabajador").prop('disabled',true)
					
				}else{
					console.log("trabajador no existe");
						}	
					  }
					})
		
 }


/*AGREGA AL USUARIO A LA BD*/

$('#btnAgregarPersona').click(function(event) {


      var nombre = $('#usuarioNuevoNombre').val();
      var apellido = $('#usuarioNuevoApellido').val();
      var fechaNacimiento = $('#fechaNacimiento').val();
      var rutPersona = $('#rutNuevoUsuario').val();
		

		if(nombre=="" || apellido =="" || fechaNacimiento=="" || rutPersona =="" ){
			alert("Favor ingresar todos los campos")
		}else{
			var data = {
			opc: 1 ,
			nombre: nombre,
			apellido: apellido,
			fechaNacimiento: fechaNacimiento,
			rutPersona: rutPersona
		   }  		
	
			$.ajax({
				
				url:"Vender.do",
				type: "POST",
				data: data,
				success: function(response){
						console.log("persona agregada a la bd")					
							}
					})
			}
		
    });






/*VALUDA RUT*/
function validaRut(campo){

	if ( campo.length == 0 ){ 
		
		alert("El rut no cumple con el formato");
			$("#rutNuevoUsuario").val("");
			deshabilitarForm(false)
			$("#btnAgregar").prop('disabled',true)
		
		return false
	}
	
	if ( campo.length < 8 ){
		
		alert("El rut no cumple con el formato");
			$("#rutNuevoUsuario").val("");
				deshabilitarForm(false)
					$("#btnAgregar").prop('disabled',true)
						return false
	}
	
	rutCompleto = campo
	campo = campo.replace('-','')
	campo = campo.replace(/\./g,'')

	var suma = 0;
	var caracteres = "1234567890kK";
	var contador = 0;   
	 
		for (var i=0; i < campo.length; i++){
			u = campo.substring(i, i + 1);
			if (caracteres.indexOf(u) != -1)
			contador ++;
		}
		
	if ( contador==0 ) { return false }
	
	var rut = campo.substring(0,campo.length-1)
	var drut = campo.substring( campo.length-1 )
	var dvr = '0';
	var mul = 2;
	
		for (i= rut.length -1 ; i >= 0; i--) {
			suma = suma + rut.charAt(i) * mul
	                if (mul == 7) 	mul = 2
			        else	mul++
		}
		
	res = suma % 11
	
	if (res==1)		dvr = 'k'
    else if (res==0) dvr = '0'
	else {
		dvi = 11-res
		dvr = dvi + ""
	}
	if (dvr != drut.toLowerCase()) { 
			deshabilitarForm(false)
		    alert("rut invaludo");
			$("#rutNuevoUsuario").val("");
		 	$("#btnAgregar").prop('disabled',true)
			
		    

		} else { 
			
			deshabilitarForm(true),
				console.log("Rut correcto"),
					$("#btnAgregar").prop('disabled',false)
						rutVenta(rutCompleto) }
			}