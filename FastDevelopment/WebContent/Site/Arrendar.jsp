<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FastDevelopment</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href="Site/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
     <link href="Site/demo/demo.css" rel="stylesheet" />
    
    <script defer src="Site/fontawesome/js/all.js"></script> 
    <link rel="stylesheet" href="Site/css/style.css" type="text/css">

 

 

</head>
<body>

	<jsp:include page="Header.jsp"/>
	
	
	
	
	<section>
	
	
	
	
	 <div class="container">
            <div class="card">
          
              <div class="col-md-8 col-centered">
                <div class="input-group">
                  <span class="input-group-text"  >Rut</span>  
                    <input type="text" class="form-control" id="rutPrestamo" onchange="validaRut(this.value)" placeholder="17652265-5" >
                </div>
              </div>


                <div class="col-md-8 col-centered">
                  <div class="input-group">
                    <span class="input-group-text" >Cod ISBN</span>  
                      <input type="text" class="form-control" onchange="validaISBN(this.value)" id="isbnArriendo" placeholder="BN544551" >
                  </div>         
                 </div>
                 

                <div class="col-md-8 text-center col-centered">
                  <div class="input-group mb-3">
                   		 <div class="input-group-prepend">
                      		<label class="input-group-text" for="inputGroupSelect01">Metodo de pago</label>
                        </div>
                           <select class="custom-select" id="selectMetodo">
                            <!--option selected id="selecMetodo">Seleccione</option-->
                            <option >Efectivo</option>
                            <option >Debito</option>
                            <option >Credito</option>
                           </select>
                   </div>
                  </div>

                              <div class="col-md-8 text-center col-centered">
                              	<div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                  	   <label class="input-group-text" for="inputGroupSelect01">Dia de entrega</label>
                                   </div>
                                   
                                      <input type="date" id="fechaNacimiento" name="fechaNacimiento"  min="1950-01-01" max="2022-12-31" >
                                  </div>
                               </div>
                                         
                    <div class="col-md-12 text-center">
                      <button class="btn btn-primary" type="button"  id="btnArrendar"  >Arrendar </button>
                    </div>
              </div>          
        </div>      


</section>

</body>

 

<script src ="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="Site/js/main.js"></script>
<script src="Site/js/vender.js"></script>
<script src="Site/js/Validaciones.js"></script>
<script src ="Site/js/arrendar.js"></script>
<script src ="Site/js/notification/js/bootstrap-msg.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

</html>