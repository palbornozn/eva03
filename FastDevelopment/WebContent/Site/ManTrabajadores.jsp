<%@page import="cl.inacap.data.Trabajador"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FastDevelopment</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href="Site/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
     <link href="Site/demo/demo.css" rel="stylesheet" />
    
    <script defer src="Site/fontawesome/js/all.js"></script> 
    <link rel="stylesheet" href="Site/css/style.css" type="text/css">
</head>

<body>

	<jsp:include page="Header.jsp"/>
    
    
   <section>
   
	    <div class="row s">
      		 <div class="col-md-12">
       		 	<h3> <span><i class="fas fa-users"></i> </span>  Trabajadores</h3>
     		 </div>
      
      		<div class="col-md-12">
         		 <input type="text" placeholder="Nombre,Apellido,etc." >
          			<button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal"> +1 </button>
      		</div>
      		
  		</div> 
   
   
   <!-- TABLA DE TRABAJADORES -->
   
   
   		<div class="col-md-12">
   		
   			<table class="table table-striped table-light">
   				<thead>
   				
   					<tr>
   						<th>RUT</th>
   						<th>Nombres</th>
   						<th>Apellidos</th>
   						<th>Editar</th>
   					</tr>
   				</thead>
   				<%  List<Trabajador> l = (List<Trabajador>) request.getAttribute("listaTrabajador"); int opt=0; %>
   				<tbody class="tbody">
   					<%  
   					
   					for(int i = 0; i<l.size();i++){
   					%>
   					<tr>
   						<td><%=l.get(i).getDNI() %></td>
   						<td><%=l.get(i).getNombre() %></td>
   						<td><%=l.get(i).getApellidos() %></td>
		   				<td><button type="button" class="btn btn-primary" data-toggle="modal" onclick="saveData()" data-target="#modalEdicion">Editar</button></td>
		   					
   					</tr>
   				
   						
   				
   					<%} %>
   				
   				</tbody>	
   			</table> 			
   		</div>
   
   <!-- MODAL DE NUEVO TRABAJADOR -->

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Complete El Formulario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
              </button>
                </div>


        <div class="modal-body">
         
          <!--   FORMULARIO  DE INGRESO NUEVO USUARIO -->
            <form class="needs-validation" id="formUsuarioNuevo" >
                <div class="form-row">
                  <div class="col-md-6 mb-3">
                    <label for="validationCustom01">Nombre</label>
                      <input type="text" class="form-control" onchange="validarNombre(this.value)" name="nombreNuevoTrabajador" id="nombreNuevoTrabajador" >
                        </div>

                  <div class="col-md-6 mb-3">
                    <label for="validationCustom02">Apellidos</label>
                      <input type="text" class="form-control" onchange="validarApellido(this.value)" name="apellidoNuevoTrabajador" id="apellidoNuevoTrabajador" >
                        </div>
                </div>

                <div class="form-row">
         
                  <div class="col-md-3 mb-3">
                    <label for="validationCustom05">Rut</label>
                     <input type="text" class="form-control" name="rutNuevoUsuario" onchange="validaTrabajador(this.value)" id="rutNuevoTrabajador" >             
                      </div>
                        </div>
                
                    <button class="btn btn-primary" id="btnAgregarTrabajador"  type="button">Agregar</button>
              </form>


        </div>
        <div class="modal-footer">

          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
           </div>
          </div>
        </div>
   

		   
   
   <!-- Modal Edicion -->
   

<!-- Modal -->
<div class="modal fade" id="modalEdicion" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edicion Trabajador</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        	<div class="col-md-3">
        		<label for="Nombre" placeholder="<%=opt %>">Nombre</label>
        		<input type="text" />
        	</div>
        	<div class="col-md-3">
        		<label for="Nombre">Apellido</label>
        		<input type="text" />
        	</div>
        	<div class="col-md-3">
        		<label for="Nombre">RUT</label>
        		<input type="text" />
        	</div>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerar</button>
        <button type="button" class="btn btn-primary">Guardar Cambios</button>
      </div>
    </div>
  </div>
</div>
   
   
   </section>





</body>
 

<script src ="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="Site/js/main.js"></script>
<script src="Site/js/vender.js"></script>
<script src="Site/js/Validaciones.js"></script>
<script src ="Site/js/notification/js/bootstrap-msg.js"></script>
<script src="Site/js/trabajadores.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

</html>