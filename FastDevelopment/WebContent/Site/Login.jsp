<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FastDevelopment</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script defer src="Site/fontawesome/js/all.js"></script> 
    
    
</head>

<style>
    
    body{

            background: url(https://images.unsplash.com/photo-1481627834876-b7833e8f5570?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MTF8fHxlbnwwfHx8fA%3D%3D&w=1000&q=80) no-repeat center fixed;
            background-size: cover;      
}

    .texto{

        color: azure;
    }

    .margin{

        align-items: center;
        justify-content: center;
        position: absolute;
        top: 40%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
</style>

<body>
    

    <div class="row">
        <div class="col-md-12 text-center margin ">

           <h1 class="texto"> FastDevelopment </h1> 
            <div class="row">
                <div class="col-md-4 offset-4">
                     <form action="">
                         <div class="col-md-12 input-group flex-nowrap">
                             <span class="input-group-text" id="addon-wrapping"><i class="fas fa-user"></i> </span>
                                <input id="username" type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="addon-wrapping">
                                    </div>
                         <div class="col-md-12 input-group flex-nowrap">
                             <span class="input-group-text" id="addon-wrapping"><i class="fas fa-key"></i></span>
                                <input id="password" type="password"  class="form-control" placeholder="Pasdword" aria-label="Pasdword" on aria-describedby="addon-wrapping">
                                    </div>      
                         <div class="col-md-12 mx-auto">
                            <button id="btn-login" type="button" onclick="verificar()" class="btn btn-warning btn-lg" style="margin-top: 10px;">Entrar</button>
                                </div>
                     </form>
                 </div>
            </div>
        </div>
    </div>



</body>

<script src ="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="Site/js/login.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>


</html>